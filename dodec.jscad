function main(){
	return dodecahedron(10);
}

function dodecahedron(height){
	var result = CSG.cube({radius: [2,2,1]});
	var subCubes = [];

	for( var i = 0; i < 5; i++ ){
		subCubes.push(result.rotateX(116.565).rotateZ(i * 72));
	}

	return result.intersect(subCubes).scale(height);
}
