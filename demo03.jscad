function main(){
	var cyl = CSG.cylinder({start: [0, 0, -10], end: [0, 0, 10], radius: 5});
	return CSG.cube({radius: 9.5})
		.subtract([CSG.cube({radius: 8.5}), cyl.union([cyl.rotateX(90), cyl.rotateY(90)])]);
}