// 3D primitives

cube(size = 10);
translate([-5, -5, -5]) cube(size = 10);
cube(size = 10, center = true);
cube(size = [10, 20, 30]);

sphere(r = 10);
sphere(r = 10, $fn = 100);
translate([10, 10, 10]) sphere(r = 10, $fn = 100);

$fn = 32;
cylinder();
cylinder(h = 10, r = 10);
cylinder(h = 2, r1 = 10, r2 = 2);

// Points define all of the points/vertices in the shape.
// Triangles is a list of triangles that connect up the points/vertices. 
polyhedron(points = [[-10, -10, 0], [-10, 10, 0], [10, 10, 0], [10, -10, 0], [0, 0, 10]]);
polyhedron(points = [[-10, -10, 0], [-10, 10, 0], [10, 10, 0], [10, -10, 0], [0, 0, 10]], triangles = [[0, 10, 2], [2, 3, 0]]);
polyhedron(points = [[-10, -10, 0], [-10, 10, 0], [10, 10, 0], [10, -10, 0], [0, 0, 10]], triangles = [[0, 1, 2], [2, 3, 0], [0, 1, 4], [1, 2, 4], [2, 3, 4], [3, 0, 4]]);

// Transformations
translate([10, 10, 10]){...} 
rotate([0, 90, 0]){...} 
scale([1, 1, 2]){...} 
mirror([1, 0, 0]){...} 

// Operations
union(){...} 
difference(){...} 
intersection(){...} 

// 2D primitives

square();
square(size = 10);
square(size = 10, center = true);
square(size = [10, 20], center = true);

circle(r = 10);
circle(r = 10, $fn = 100);
translate([10, 10]) circle(r = 10, $fn = 100);

polygon(points = [[-10, -10], [-10, 10], [10, 10], [10, -10]]);
polygon(points = [[-10, -10], [-15, 0], [-10, 10], [0, 15], [10, 10], [15, 0], [10, -10], [0, -15]]);
linear_extrude(height = 10) polygon(points = [[-10, -10], [-15, 0], [-10, 10], [0, 15], [10, 10], [15, 0], [10, -10], [0, -15]]);
linear_extrude(height = 10, twist = 360) polygon(points = [[-10, -10], [-15, 0], [-10, 10], [0, 15], [10, 10], [15, 0], [10, -10], [0, -15]]);
