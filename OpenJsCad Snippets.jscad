// 3D primitives

CSG.cube({radius: 10});
CSG.cube({radius: 10, center: 10});
CSG.cube({radius: [10, 20, 30]});

CSG.sphere({radius: 10});
CSG.sphere({radius: 10, resolution: 100});
CSG.sphere({radius: 10, center: 10, resolution: 100});

CSG.cylinder({start: 0, end: 10, radius: 10});
CSG.cylinder({start: [0, 0, 0], end: [0, 0, 10], radius: 10});
CSG.cylinder({start: [0, 0, 0], end: [0, 0, 10], radius: 10, resolution: 64});
CSG.cylinder({start: [0, 0, 0], end: [0, 0, 10], radiusStart: 10, radiusEnd: 20, resolution: 64});

// Transformations
object.translate([10, 10, 10])... 
object.rotate<XYZ>(90) 
object.scale([1, 1, 2]) 
object.mirrored<XYZ>() 

// Operations
object1.union(object2)... 
object1.union([object2, object3, objectn])... 
object1.subtract(object2) 
object1.subtract([object2, object3, objectn])... 
object1.intersect(object2) 
object1.intersect([object2, object3, objectn])... 

// 2D Primitives

CAG.rectangle({radius: 10});
CAG.rectangle({radius: 10, center: 10});
CAG.rectangle({radius: [10, 20]});

CAG.circle({radius: 10});
CAG.circle({radius: 10, resolution: 100});
CAG.circle({radius: 10, center: 10, resolution: 100});

CAG.fromPoints([[-10, -10], [-10, 10], [10, 10], [10, -10]]);
CAG.fromPoints([[-10, -10], [-15, 0], [-10, 10], [0, 15], [10, 10], [15, 0], [10, -10], [0, -15]]);
CAG.fromPoints([[-10, -10], [-15, 0], [-10, 10], [0, 15], [10, 10], [15, 0], [10, -10], [0, -15]]).extrude({offset: 10});
CAG.fromPoints([[-10, -10], [-15, 0], [-10, 10], [0, 15], [10, 10], [15, 0], [10, -10], [0, -15]]).extrude({offset: [0, 0, 10]});
CAG.fromPoints([[-10, -10], [-15, 0], [-10, 10], [0, 15], [10, 10], [15, 0], [10, -10], [0, -15]]).extrude({offset: [0, 0, 10], twistangle: 360, twiststeps: 10});
