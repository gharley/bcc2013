function main(){
	var cyl = CSG.cylinder({start: [0, 0, -20.1], end: [0, 0, 20.1], radius: 5});
	return CSG.cube({radius: 20})
		.subtract([CSG.sphere({radius: 18.5}), cyl.union([cyl.rotateX(90), cyl.rotateY(90)])])
		.union(CSG.sphere({radius: 10}).translate([0, 0, -10]));
}
