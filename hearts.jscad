function getParameterDefinitions(){
	return [
		{name: 'radius', type: 'float', default: 30, caption: 'Radius of outer heart lobe'},
		{name: 'width', type: 'float', default: 8, caption: 'Width of heart face'},
		{name: 'height', type: 'float', default: 10, caption: 'Thickness of hearts'},
		{name: 'resolution', type: 'int', default: 180, caption: 'Resolution'}
	];
}

function main(options){
	options.holeDepth = options.width / 2;

	var result = heart3D(options).union(hanger(options).translate([0, options.radius + options.holeDepth - 1, options.height / 2]));
	
	options.pinHeight = options.width / 2 + 1;
	options.radius -= options.width + 2;
	result = result.union(heart3D(options));
	
	options.radius -= options.width + 2;
	options.holeDepth = 0;
	options.knockout = false;
	result = result.union(heart3D(options));

	return result;
}

function hanger(options){
	options = options || {};
	var width = options.width || 8;
	var height = options.height || 10;
	var resolution = options.resolution || 180;
	var result = CSG.cylinder({
		start: [0, 0, -height / 2],
		end: [0, 0, height / 2],
		radius: width,
		resolution: resolution / 4
	});
	
	result = result.subtract(CSG.cylinder({
		start: [0, 0, -height / 2],
		end: [0, 0, height / 2],
		radius: width / 2,
		resolution: resolution / 4
	}));
	
	return result;
}

function heart3D(options){
	options = options || {};
	var radius = options.radius || 30;
	var height = options.height || 10;
	var width = options.width || 8;
	var holeDepth = options.holeDepth || 0;
	var pinHeight = options.pinHeight || 0;
	var knockout = !(options.knockout === false);
	var result = heart({radius: radius}).extrude({offset: [0, 0, height]});

	if( 0 != pinHeight ){
		result = result.union(
			CSG.cylinder({
				start: [0, -radius * 2 - pinHeight * 2, 0],
				end: [0, radius + pinHeight, 0],
				radius: height / 4,
				resolution: 16
			}).translate([0, 0, height / 2])
		);
	}

	if( 0 != holeDepth ){
		result = result.subtract(
			CSG.cylinder({
				start: [0, -radius * 2 + holeDepth * 2, 0],
				end: [0, radius - holeDepth, 0],
				radius: height / 4 + 1,
				resolution: 16
			}).translate([0, 0, height / 2])
		);
	}

	if( knockout ){
		result = result.subtract(heart({radius: radius - width}).extrude({offset: [0, 0, height]}));
	}

	return result;
}

function heart(options){
	options = options || {};
	var radius = options.radius || 30;
	var resolution = options.resolution || 180;
	var result = CSG.Path2D.arc({
		center: [-radius / 2, 0, 0],
		radius: radius,
		startangle: 0,
		endangle: 220, 
		resolution: resolution
	});
	
	result = result.appendPoint([0, -radius * 2]);
	result = result.close();
	result = result.innerToCAG();
	result = result.union(result.mirroredX());
	
	return result;
}