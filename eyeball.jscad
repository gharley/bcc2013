function main(){
	return eyeball().subtract(CSG.cylinder({start: [0, 0, -1], end: [0, 0, 16], radius: 5}));
}

function eyeball(){
	var result = gimbleRing({radius: 30, holeDepth: 3});

	result = result.union(gimbleRing({radius: 23, holeDepth: 3, pinHeight: 3.5}).rotateX(90));
	result = result.union(gimbleRing({radius: 16, pinHeight: 3.5, trimFront: false}));

	return result.rotateY(-90);
}

function gimbleRing(args){
	args = args || {};
	args.height = args.height || 10;
	args.width = args.width || 5;
	args.radius = args.radius || 30;
	args.holeDepth = args.holeDepth || 0;
	args.pinHeight = args.pinHeight || 0;
	args.trimFront = args.trimFront === false ? args.trimFront : true;
	args.trimBack = args.trimBack === false ? args.trimBack : true;

	var outerSphere = CSG.sphere({center: [0, 0, 0], radius: args.radius, resolution: 32});
	var innerSphere = CSG.sphere({center: [0, 0, 0], radius: args.radius - args.width, resolution: 32});
	var result  = outerSphere;
	
	if( 0 != args.holeDepth ){
		var holeCyl = CSG.cylinder({start: [0, -(args.radius - args.holeDepth), 0], end: [0, args.radius - args.holeDepth, 0], radius: (args.height / 4) + 1});
		
		result = result.subtract(holeCyl.setColor([0, 1, 0]));
	}
	
	if( 0 != args.pinHeight ){
		var pinCyl = CSG.cylinder({start: [0, -(args.radius + args.pinHeight), 0], end: [0, args.radius + args.pinHeight, 0], radius: args.height / 4});
		
		result = result.union(pinCyl.rotateX(90).setColor([1, 0, 0]));
	}
	
	result  = result.subtract(innerSphere);
	
	if( args.trimFront || args.trimBack ){
		var slicePlane = CSG.Plane.fromNormalAndPoint([args.radius, 0, 0], [args.height / 2, 0, 0]);

		result = args.trimFront ? result.cutByPlane(slicePlane) : result;
		result = args.trimBack ? result.cutByPlane(slicePlane.translate([-args.height, 0, 0]).flipped()) : result;
	}

	return result;
}
