function main(){
	var cube = CSG.cube({radius: 10});
	var sphere = CSG.sphere({radius: 10, resolution: 50}).translate(10);
	var cylinder = CSG.cylinder({start: [0, 0, 0], end: [0, 0, 10], radius: 10});
	var cone = CSG.cylinder({start: [0, 0, 0], end: [0, 0, 20], radiusStart: 10, radiusEnd: 2});
	
	return cube;
//	return cube.union([sphere, cone]);
//	return cube.subtract([sphere, cone]);
//	return cube.intersect([sphere, cone]);
}
