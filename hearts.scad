$fn = 100;

union(){
	heart3D(radius = 30, width = 8, holeDepth = 4);
	heart3D(radius = 20, width = 8, holeDepth = 4, pinHeight = 5);
	heart3D(radius = 10, width = 8, pinHeight = 5, knockOut = false);
	difference(){
		translate([0, 31, -5]) color([0, 0, .5, .5]) cylinder(10, 5, 5);
		translate([0, 31, -6]) cylinder(12, 2, 2);
	}
}

module heart3D(radius = 30, height = 10, width = 5, holeDepth = 0, pinHeight = 0, knockOut = true){
	difference(){
		difference(){
			union(){
				linear_extrude(center = true, height = height) heart(radius);
				if( 0 != pinHeight ){
					rotate([90, 0, 0]) translate([0, 0, -(radius + pinHeight)]) color([0, .5, 0, .5]) cylinder(radius * 3 + pinHeight * 2 + width * .25, height / 4, height / 4, false);
				}
			}
			if( knockOut ){
				linear_extrude(center = true, height = height + 2) heart(radius - width);
			}
		}
		if( 0 != holeDepth ){
			rotate([90, 0, 0]) translate([0, 0, -radius + holeDepth]) color([.5, 0, 0, .3]) cylinder(radius * 3 - width - holeDepth * 2, height / 4 + 1, height / 4 + 1, false);
		}
	}
}

module heart(radius = 30){
	translate([-radius / 2, 0, 0]) difference(){
		hull(){
			union(){
				circle(radius);
				translate([radius, 0, 0]) circle(radius);
				polygon([[-radius, 0], [radius * 2, 0], [radius / 2, -radius * 2]]);
			}
		}
		difference(){
			translate([-radius, 0, 0]) square([radius * 3, radius]);
			union(){
				circle(radius);
				translate([radius, 0, 0]) circle(radius);
			}
		}
	}
}